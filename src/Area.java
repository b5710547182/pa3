/**
 * The enum for the different Unit in Area, this class will be used to convert to different unit.
 * @author Thanawit Gerdprasert.
 *
 */
public enum Area implements Unit {

	SQUAREWA ( "SQUAREWA", 4.0 ),
	SQUAREYARD ( "SQUAREYARD" , 0.8361),
	SQUAREFOOT ( "SQUAREFOOT" , 0.09290),
	SQUAREINCH ( "SQUAREINCH" , 0.0006451 ),
	SQUAREMILE ( " SQUAREMILE " , 2589988.110),
	SQUAREKILOMETER ( "SQUAREKILOMETER" , 1000000.0),
	SQUAREMETER( "SQUAREMETER" , 1.0);

	/**
	 * the name of current object.
	 */
	public final String name;
	/**
	 * the value of current object.
	 */
	public final double value;
	/**
	 * initialize the name and the value.
	 * @param name of the Unit.
	 * @param value of the unit compared to Square Meter.
	 */
	Area (String name,double value)
	{
		this.name = name;
		this.value = value;
	}
	@Override
	public double getValue()
	{
		return this.value;
	}

	@Override
	public String toString()
	{
		return this.name;
	}
	@Override
	public double convertTo ( double amount ,Unit unit) {
		double returnNumber = (amount*this.getValue())/unit.getValue();
		return returnNumber;
	}

	
}
