import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * This class will generate the User Interface of Converter Program.
 * @author Thanawit Gerdprasert.
 */
public class ConverterUI extends JFrame implements Runnable{

	private UnitConverter temp;
	private JTextField fromToArea;
	private JComboBox<Unit> fromToType;
	private JTextField convertToArea;
	private JComboBox<Unit> convertToType;
	private JButton clearButton;
	private JButton convertButton;
	private double amount = 0 ;
	private Unit tempFromToType = null;
	private Unit tempConvertToType = null;
	
	/**
	 * Initialize the User Interface and the Unit Converter.
	 */
	public ConverterUI()
	{
		super("Welcome to Converter Program");
		temp = new UnitConverter();
		initComponent();
	}
	/**
	 * Initialize the GUI component.
	 */
	public void initComponent()
	{
		this.setLayout(new BorderLayout());
		Container top = new Container();
		Container bottom = new Container();

		JMenuBar temp = new JMenuBar();
		JMenu component = new JMenu("Unit Type");
		JMenuItem LengthMenu = new JMenuItem("Length");
		JMenuItem AreaMenu = new JMenuItem("Area");
		JMenuItem WeightMenu = new JMenuItem("Weight");
		JMenuItem exitMenu = new JMenuItem("Exit");
		
		
		component.add(LengthMenu);
		component.add(AreaMenu);
		component.add(WeightMenu);
		
		component.addSeparator();
		
		component.add(exitMenu);
		
		exitMenu.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(ABORT);
			}
		});
		
		
		temp.add(component);
		top.add(temp);

		top.setLayout(new FlowLayout(FlowLayout.LEFT));
		bottom.setLayout(new FlowLayout());
		
		fromToArea = new JTextField(10);
		fromToArea.addActionListener(new ConvertButtonAction());
		bottom.add(fromToArea);
		
		
		fromToType = new JComboBox<Unit>(Length.values());
		bottom.add(fromToType);
		
		JLabel equalSign = new JLabel("=");
		bottom.add(equalSign);
		
		convertToArea = new JTextField(10);
		bottom.add( convertToArea );
		
		convertToType = new JComboBox<Unit>(Length.values());
		bottom.add(convertToType);
		
		convertButton = new JButton("CONVERT");
		convertButton.addActionListener(new ConvertButtonAction());
		bottom.add(convertButton);
		
		clearButton = new JButton("CLEAR");
		clearButton.addActionListener(new ClearButtonAction());
		bottom.add(clearButton);
		
		Font font = new Font("Arial",Font.PLAIN,24);
		this.setFont(font);

		this.add(top,BorderLayout.NORTH);
		this.add(bottom,BorderLayout.SOUTH);
		

		
		LengthMenu.addActionListener(new ConvertAnyType(UnitType.LengthUnit));
		AreaMenu.addActionListener(new ConvertAnyType(UnitType.AreaUnit));
		WeightMenu.addActionListener(new ConvertAnyType(UnitType.WeightUnit));
		
		
		
		
		
		this.pack();
	}
	/**
	 * Called this method to do calculation that convert one unit to other.
	 */
	public void doingCalculation()
	{
		try
		{
		amount = Double.valueOf(fromToArea.getText());
		tempFromToType = (Unit)fromToType.getSelectedItem();
		tempConvertToType = (Unit)convertToType.getSelectedItem();
		convertToArea.setText(temp.convert(amount, tempFromToType, tempConvertToType)+"");
		}
		catch(NumberFormatException event1)
		{
			JOptionPane.showMessageDialog(new JFrame(), "PLEASE INPUT THE NUMBER","ERROR",JOptionPane.ERROR_MESSAGE );
		}
	}
	/**
	 * Run the GUI and set close operation.
	 */
	public void run()
	{
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	/**
	 * Action when the convert button is pressed.
	 * @author Thanawit Gerdprasert.
	 *
	 */
	class ConvertButtonAction implements ActionListener
	{
		/**
		 * Perform the action that will convert different type of Unit.
		 */
		public void actionPerformed(ActionEvent e) {
			
			if(convertToArea.getText().equals("")&&fromToArea.getText().equals(""))
			{
				JOptionPane.showMessageDialog(new JFrame(), "BOTH FIELD IS EMPTY","ERROR",JOptionPane.ERROR_MESSAGE );
			}
			else if(convertToArea.getText().equals(""))
			{
				doingCalculation();
			}
			else if(fromToArea.getText().equals(""))
			{
				try
				{
				amount = Double.valueOf(convertToArea.getText());
				tempFromToType = (Unit)convertToType.getSelectedItem();
				tempConvertToType = (Unit)fromToType.getSelectedItem();
				fromToArea.setText(temp.convert(amount, tempFromToType, tempConvertToType)+"");
				}
				catch(NumberFormatException event1)
				{
					JOptionPane.showMessageDialog(new JFrame(), "PLEASE INPUT THE NUMBER","ERROR",JOptionPane.ERROR_MESSAGE );
				}
			}
		}
	}
	/**
	 * When Enter button is pressed, perform the action that will convert different type of Unit.
	 * @author Thanawit Gerdprasert
	 *
	 */
	 class EnterKeyPress implements KeyListener{
		 /**
		  * When the key is type( in this case do nothing).
		  * @param e the key event.
		  */
		public void keyTyped(KeyEvent e) {
		}
		
		/**
		 * When the Enter key is pressed, convert the Unit.
		 * @param e is the parameter of key that pressed.
		 */
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode()==KeyEvent.VK_ENTER)
			{
				doingCalculation();
			}
			
		}
		/**
		  * When the key is released( in this case do nothing).
		  * @param e the key event.
		  */
		public void keyReleased(KeyEvent e) {
		}	
	}
	 /**
	  * Clear the alphabet or number in the Text Field.
	  * @author Thanawit Gerdprasert.
	  *
	  */
	class ClearButtonAction implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			fromToArea.setText("");
			convertToArea.setText("");
		}
	}
	/**
	 * Use to change the type of unit that will be converted from JMenuBar.
	 * @author Thanawit Gerdprasert.
	 *
	 */
	class ConvertAnyType implements ActionListener
	{
		private UnitType type;
		public ConvertAnyType(UnitType type)
		{
			this.type = type;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
	
			
			fromToArea.setText("");
			fromToType.removeAllItems();
			convertToArea.setText("");
			convertToType.removeAllItems();

			for(Unit u : type.getUnitTypeUnit())
			{
				fromToType.addItem(u);
				convertToType.addItem(u);
			}
			
			
			
			
			pack();
			
		}
	}
	
	
}
