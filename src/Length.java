/**
 * The enum for the different Unit in Length, this class will be used to convert to different unit.
 * @author Thanawit Gerdprasert.
 *
 */
public enum Length implements Unit {

	WA ( "WA", 2.0 ),
	MICRON ("MICRON" , 1.0E-6),
	YARD ( "YARD" , 0.9144),
	FOOT ( "FOOT" , 0.3048),
	INCH ( "INCH" , 0.0254 ),
	MILE ( " MILE " , 1609.344),
	KILOMETER ( "KILO" , 1000.0),
	METER( "METER" , 1.0);

	/**
	 * the name of current object.
	 */
	public final String name;
	/**
	 * the value of current object.
	 */
	public final double value;
	/**
	 * initialize the name and the value.
	 * @param name of the Unit.
	 * @param value of the unit compared to meter.
	 */
	Length(String name,double value)
	{
		this.name = name;
		this.value = value;
	}
	@Override
	public double getValue()
	{
		return this.value;
	}

	@Override
	public String toString()
	{
		return this.name;
	}
	@Override
	public double convertTo ( double amount ,Unit unit) {
		double returnNumber = (amount*this.getValue())/unit.getValue();
		return returnNumber;
	}

	
}
