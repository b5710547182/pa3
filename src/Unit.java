/**
 * This interface is used when it need to be convert to other type of Unit.
 * @author Thanawit Gerdprasert.
 *
 */
public interface Unit {
	
	/**
	 * Convert the Unit from itself into received Unit.
	 * @param amount of value that will be converted.
	 * @param unit that will be converted to.
	 * @return amount after convert.
	 */
	public double convertTo(double amount ,Unit unit   ) ;
	
	/**
	 * get the value of the unit compare to Meter.
	 * @return the value of unit as Meter.
	 */
	public double getValue();
	
	/**
	 * Get the String of the unit.
	 * @return the String that identify its type.
	 */
	public String toString();
}
