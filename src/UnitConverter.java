/**
 * Main class for using the convert and get the current Unit.
 * @author Thanawit Gerdprasert,
 *
 */
public class UnitConverter {

	
	/**
	 * convert the data from the fromTo type to toUnit type.
	 * @param amount that will be convert.
	 * @param fromTo the unit before convert.
	 * @param toUnit the unit after convert.
	 * @return the amount after convert.
	 */
	public double convert (double amount , Unit fromTo , Unit toUnit)
	{
		return fromTo.convertTo(amount, toUnit);
	}
	/**
	 * return the overall unit in the current series.
	 * @return the Array of Unit containing all data in the Unit type.
	 */
	public Unit[] getUnitType(UnitType type)
	{
		UnitType temp = type;
		return temp.getUnitTypeUnit();
	}
}
