/**
 * This class contain all unit type that will be used for the converter.
 * @author Thanawit Gerdprasert.
 *
 */
public enum UnitType {

	LengthUnit (Length.values()),
	AreaUnit (Area.values()),
	WeightUnit (Weight.values());
	
	private Unit[] type;
	/**
	 * Contructor for this enum that will initialize its type.
	 * @param type of the unit that will be used.
	 */
	UnitType(Unit[] type)
	{
		this.type = type;
	}
	/**
	 * return unit type from the enum.
	 * @return unit type of the current enum.
	 */
	public Unit[] getUnitTypeUnit()
	{
		return type;
	}
	
	/**
	 * set the Unit type of the current enum.
	 * @param type of unit that it will be change to.
	 */
	public void setUnitType(Unit[] type)
	{
		this.type = type;
	}
	
}
