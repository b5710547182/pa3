/**
 * This enum contain the data of the Weight Unit and can convert Weight unit to other Weight unit.
 * @author Thanawit Gerdprasert.
 *
 */
public enum Weight implements Unit{

	GRAM("GRAM",1.0),
	KILOGRAM("KILOGRAM",1000.0),
	POUND("POUND" , 453.59 ),
	THANG("THANG" , 15000.0),
	NEWTON("NEWTON" , 101.97 );

	/**
	 * value of the current unit.
	 */
	public final double value;

	/**
	 * name of the current unit.
	 */
	public final String name;
	/**
	 * initialize the name and the value.
	 * @param name of the Unit.
	 * @param value of the unit compared to meter.
	 */
	Weight(String name , double value)
	{
		this.name = name;
		this.value = value;
	}


	@Override
	public double convertTo(double amount, Unit unit) {

		return (this.value*amount)/unit.getValue();
	}

	@Override
	public double getValue() {

		return this.value;
	}

	@Override
	public String toString()
	{
		return this.name;
	}

}
